# [1.1.0](https://gitlab.com/ragingpastry/piperci-demo/compare/v1.0.1...v1.1.0) (2019-07-31)


### Features

* add super cool feature ([0f6bb1c](https://gitlab.com/ragingpastry/piperci-demo/commit/0f6bb1c))

## [1.0.1](https://gitlab.com/ragingpastry/piperci-demo/compare/v1.0.0...v1.0.1) (2019-07-31)


### Bug Fixes

* add changelog to git ([a3908a5](https://gitlab.com/ragingpastry/piperci-demo/commit/a3908a5))
