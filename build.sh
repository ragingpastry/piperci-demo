CONTAINER_BUILDER_IMAGE="tox-faas:builder"
CONTAINER_TEST_IMAGE="tox-faas:test"

# This is a super cool feature I just added

docker pull $CONTAINER_BUILDER_IMAGE || true
docker build \
  --target builder \
  --cache-from $CONTAINER_BUILDER_IMAGE \
  -t $CONTAINER_BUILDER_IMAGE \
  "."

docker pull $CONTAINER_TEST_IMAGE || true
docker build --pull \
  --cache-from $CONTAINER_BUILDER_IMAGE \
  --cache-from $CONTAINER_TEST_IMAGE \
  -t $CONTAINER_TEST_IMAGE \
  "."

docker push $CONTAINER_TEST_IMAGE
docker push $CONTAINER_BUILDER_IMAGE

docker run $CONTAINER_TEST_IMAGE
